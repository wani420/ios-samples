//
//  TableViewCell.m
//  SelfSizingTableView
//
//  Created by CBLUE on 2017/6/3.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        _label = [UILabel new];
        _label.numberOfLines = 0;
        _label.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_label];
        
        _descLabelLeft = [UILabel new];
        _descLabelLeft.text = @"11111";
        _descLabelLeft.backgroundColor = [UIColor greenColor];
        _descLabelLeft.numberOfLines = 0;
        _descLabelLeft.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_descLabelLeft];

        _descLabelRight = [UILabel new];
        _descLabelRight.numberOfLines = 0;
        _descLabelRight.text = @"2222\n123123\n123123";
        _descLabelRight.backgroundColor = [UIColor redColor];
        _descLabelRight.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_descLabelRight];
        
        NSDictionary *views = @{@"label" : _label,
                                @"descLabelLeft" : _descLabelLeft,
                                @"descLabelRight" : _descLabelRight};
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label]-|" options:0 metrics:nil views:views]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[descLabelLeft]-[descLabelRight(==descLabelLeft)]-|" options:0 metrics:nil views:views]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[label]" options:0 metrics:nil views:views]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[label]-[descLabelLeft]-|" options:0 metrics:nil views:views]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[label]-[descLabelRight]-|" options:0 metrics:nil views:views]];
    }
    
    return self;
}

@end
