//
//  TableViewCell.h
//  SelfSizingTableView
//
//  Created by CBLUE on 2017/6/3.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UILabel *descLabelLeft;
@property (nonatomic, strong) UILabel *descLabelRight;
@end
