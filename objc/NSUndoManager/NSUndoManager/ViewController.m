//
//  ViewController.m
//  NSUndoManager
//
//  Created by Wani on 2017/6/19.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (assign, nonatomic) NSInteger numberValue;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.numberValue = 0;
}

- (void)setNumberValue:(NSInteger)numberValue
{
    // It's important that the value here should be the `previous value`,
    // which is `_numberValue` rather than the new value `number`
    [[self.undoManager prepareWithInvocationTarget:self] setNumberValue:_numberValue];
    [self.undoManager setActionName:@"Undo"];
    
    _numberValue = numberValue;
    self.valueLabel.text = [NSString stringWithFormat:@"value = %@", @(numberValue)];
}

- (IBAction)handleUndo:(id)sender
{
    if ([self.undoManager canUndo]) {
        [self.undoManager undo];
    }
}

- (IBAction)handleRedo:(id)sender
{
    if ([self.undoManager canRedo]) {
        [self.undoManager redo];
    }
}

- (IBAction)handlePlus:(id)sender
{
    self.numberValue = self.numberValue + 1;
}

@end
