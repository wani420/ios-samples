//
//  main.m
//  3DTouch
//
//  Created by CBLUE on 2017/5/15.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
