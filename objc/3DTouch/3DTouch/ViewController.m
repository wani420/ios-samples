//
//  ViewController.m
//  3DTouch
//
//  Created by CBLUE on 2017/5/15.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import "ViewController.h"
#import "DestinationViewController.h"

@interface ViewController () <UIViewControllerPreviewingDelegate>
@property (weak, nonatomic) IBOutlet UIButton *previewButton;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable) {
        [self registerForPreviewingWithDelegate:self sourceView:self.previewButton];
    }
}

#pragma mark - UIViewControllerPreviewingDelegate

- (UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext
              viewControllerForLocation:(CGPoint)location {
    
    DestinationViewController *viewController = [[DestinationViewController alloc]
                                                 initWithTitle:@"Preview with 3D Touch"];
    
    // Set width 0 as default
    viewController.preferredContentSize = CGSizeMake(0, 300);
    
    // Set the source rect to the cell frame, so surrounding elements are blurred.
    previewingContext.sourceRect = self.previewButton.frame;
    
    return viewController;
}

- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext
     commitViewController:(UIViewController *)viewControllerToCommit
{
    [self showViewController:viewControllerToCommit sender:self];
}

@end
