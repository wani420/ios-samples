//
//  DestinationViewController.m
//  3DTouch
//
//  Created by CBLUE on 2017/5/15.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import "DestinationViewController.h"

@interface DestinationViewController ()

@end

@implementation DestinationViewController

- (instancetype)initWithTitle:(NSString *)title
{
    self = [super init];

    if (self) {
        self.title = title;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.text = self.title;
//    [titleLabel sizeToFit];
    titleLabel.backgroundColor = [UIColor redColor];
//    titleLabel.center = self.view.center;
    
    [self.view addSubview:titleLabel];
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *views = @{@"titleLabel": titleLabel};
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[titleLabel]-|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[titleLabel]-|" options:0 metrics:nil views:views]];

}

@end
