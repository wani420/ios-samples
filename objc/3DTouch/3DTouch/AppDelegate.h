//
//  AppDelegate.h
//  3DTouch
//
//  Created by CBLUE on 2017/5/15.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

