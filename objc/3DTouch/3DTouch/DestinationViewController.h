//
//  DestinationViewController.h
//  3DTouch
//
//  Created by CBLUE on 2017/5/15.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DestinationViewController : UIViewController
- (instancetype)initWithTitle:(NSString *)title;
@end
