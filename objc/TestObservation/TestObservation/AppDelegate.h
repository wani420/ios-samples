//
//  AppDelegate.h
//  TestObservation
//
//  Created by CBLUE on 2017/5/22.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

