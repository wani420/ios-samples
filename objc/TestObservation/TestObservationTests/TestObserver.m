//
//  TestObserver.m
//  TestObservation
//
//  Created by CBLUE on 2017/5/22.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import "TestObserver.h"

@implementation TestObserver

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        [[XCTestObservationCenter sharedTestObservationCenter]
         addTestObserver:self];
    }
    
    return self;
}

#pragma mark - XCTestObservation

- (void)testBundleWillStart:(NSBundle *)testBundle
{
    NSLog(@"testBundleWillStart");
}

@end
