//
//  TestObserver.h
//  TestObservation
//
//  Created by CBLUE on 2017/5/22.
//  Copyright © 2017年 Wani. All rights reserved.
//

#import <Foundation/Foundation.h>
@import XCTest;

@interface TestObserver : NSObject <XCTestObservation>

@end
