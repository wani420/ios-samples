//
//  SecondViewController.m
//  DragDownDimiss
//
//  Created by CBLUE on 4/14/17.
//  Copyright © 2017 Wani. All rights reserved.
//

#import "SecondViewController.h"
#import "PullDownDismissCardTransition.h"
#import <objc/runtime.h>

@implementation UIScrollView(Helper)

- (void)disableTopBounce
{
    [self addObserver:self
           forKeyPath:NSStringFromSelector(@selector(contentOffset))
              options:NSKeyValueObservingOptionNew
              context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(contentOffset))]) {
        CGPoint contentOffset = [change[NSKeyValueChangeNewKey] CGPointValue];
        if (contentOffset.y + self.contentInset.top < 0) {
            contentOffset.y = -(self.contentInset.top);
            self.contentOffset = contentOffset;
        }
    }
}

+ (void)load
{
    Method origMethod = class_getInstanceMethod([self class], NSSelectorFromString(@"dealloc"));
    Method newMethod = class_getInstanceMethod([self class], @selector(my_dealloc));
    method_exchangeImplementations(origMethod, newMethod);
}

- (void)my_dealloc
{
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(contentOffset))];
    [self my_dealloc];
}
@end

@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonnull) PullDownDismissCardTransition *transition;
@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.textView disableTopBounce];
    
    self.transition = [[PullDownDismissCardTransition alloc] initWithTargetViewController:self];
    self.transitioningDelegate = self.transition;
    self.transition.targetScrollViews = @[self.textView];
}

- (IBAction)actionDismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    NSLog(@"touchesBegan");
}

@end

