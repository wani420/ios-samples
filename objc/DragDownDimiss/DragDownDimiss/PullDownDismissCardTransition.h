//
//  PullDownTransition.h
//  DragDownDimiss
//
//  Created by CBLUE on 4/14/17.
//  Copyright © 2017 Wani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PullDownDismissCardTransition : NSObject <UIViewControllerTransitioningDelegate>
- (instancetype)initWithTargetViewController:(UIViewController *)targetViewController;
@property (nonatomic, copy) void (^didDimissByTapTop)();
@property (nonatomic, copy) void (^didDismissByPullDown)();
@property (nonatomic, strong) NSArray<UIScrollView *> *targetScrollViews;
@property (nonatomic, assign) BOOL enabled;
@end

@interface CardTransitionAnimator : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic, assign) BOOL appearing;
@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, copy) void (^didDimissByTapTop)();
@end

@interface PullDownTransitionInteractor : UIPercentDrivenInteractiveTransition
@property (nonatomic, assign) BOOL interactionInProgress;
@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, copy) void (^didDismissByPullDown)();
@property (nonatomic, strong) NSArray<UIScrollView *> *targetScrollViews;
+ (instancetype)interactorWithTargetViewController:(UIViewController *)targetViewController;
@end
