//
//  ViewController.swift
//  DragAndDrop
//
//  Created by CBLUE on 2017/8/10.
//  Copyright © 2017年 Wani. All rights reserved.
//

import UIKit
import MobileCoreServices

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dropInteraction = UIDropInteraction(delegate: self)
        view.addInteraction(dropInteraction)
        
        let dragInteraction = UIDragInteraction(delegate: self)
        view.addInteraction(dragInteraction)
    }
}

extension ViewController : UIDropInteractionDelegate {
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.hasItemsConforming(toTypeIdentifiers: [kUTTypeImage as String]) && session.items.count == 1
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        
        let dropLocation = session.location(in: view)

        let operation: UIDropOperation
        
        if view.frame.contains(dropLocation) {
            /*
             If you add in-app drag-and-drop support for the .move operation,
             you must write code to coordinate between the drag interaction
             delegate and the drop interaction delegate.
             */
            operation = session.localDragSession == nil ? .copy : .move
        } else {
            // Do not allow dropping outside of the image view.
            operation = .cancel
        }
        
        return UIDropProposal(operation: operation)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        session.loadObjects(ofClass: UIImage.self) { imageItems in
            let images = imageItems as! [UIImage]
            let image = images.first!
            let dropLocation = session.location(in: self.view)
            
            DispatchQueue.main.async {
                let imageView = UIImageView(image: image)
                let size = image.size.scaleToFitDimension(dimension: 100)
                imageView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
                imageView.contentMode = .scaleAspectFit
                imageView.center = dropLocation
                self.view.addSubview(imageView)
                
                if let localDragSession = session.localDragSession {
                    let originalImageView = localDragSession.localContext as? UIImageView
                    originalImageView?.removeFromSuperview()
                }
            }
        }
    }
}

extension ViewController: UIDragInteractionDelegate {
    
    func imageViewAtLocation(location:CGPoint) -> UIImageView? {
        var imageView:UIImageView?
        view.subviews.forEach { v in
            if v.frame.contains(location),
                let v = v as? UIImageView {
                imageView = v
            }
        }
        return imageView
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        let dragLocation = session.location(in: view)
        
        var dragItems = [UIDragItem]()
        
        if let imageView = imageViewAtLocation(location: dragLocation),
            let image = imageView.image {
            let provider = NSItemProvider(object: image)
            let item = UIDragItem(itemProvider: provider)
            item.localObject = image
            dragItems.append(item)
            session.localContext = imageView
        }
        
        return dragItems
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
        guard let image = item.localObject as? UIImage else { return nil }

        let dragLocation = session.location(in: view)
      
        // Create a new view to display the image as a drag preview.
        let previewImageView = UIImageView(image: image)
        previewImageView.contentMode = .scaleAspectFit
        let size = image.size.scaleToFitDimension(dimension: 100)
        previewImageView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        previewImageView.layer.borderWidth = 5
        previewImageView.layer.borderColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        
        // Provide a custom targeted drag preview.
        let target = UIDragPreviewTarget(container: view, center: dragLocation)
        
        return UITargetedDragPreview(view: previewImageView, parameters: UIDragPreviewParameters(), target: target)
    }
}

extension CGSize {
    func scaleToFitDimension(dimension:CGFloat) -> CGSize {
        let scale = min(dimension/width, dimension/height)
        return CGSize(width: width * scale, height: height * scale)
    }
}
