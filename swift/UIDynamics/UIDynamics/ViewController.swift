//
//  ViewController.swift
//  UIDynamics
//
//  Created by CBLUE on 2017/7/3.
//  Copyright © 2017年 Wani. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var animator:UIDynamicAnimator!
    var gravity:UIGravityBehavior!
    var collision:UICollisionBehavior!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let square = UIView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
        square.backgroundColor = UIColor.gray
        view.addSubview(square)
        
        let barrier = UIView(frame: CGRect(x: 0, y: 300, width: 130, height: 20))
        barrier.backgroundColor = UIColor.red
        view.addSubview(barrier)
        
        animator = UIDynamicAnimator(referenceView: view)
        gravity = UIGravityBehavior(items: [square])
        collision = UICollisionBehavior(items: [square])
        collision.translatesReferenceBoundsIntoBoundary = true
        collision.addBoundary(withIdentifier: "barrier" as NSString, from: barrier.frame.origin, to: CGPoint(x: barrier.frame.maxX, y: barrier.frame.minY))
        collision.collisionDelegate = self
        
        animator.addBehavior(gravity)
        animator.addBehavior(collision)
    }
}

extension ViewController: UICollisionBehaviorDelegate {
    func collisionBehavior(_ behavior: UICollisionBehavior, beganContactFor item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying?, at p: CGPoint) {
        
        if let v = item as? UIView {
            v.backgroundColor = UIColor.yellow
            UIView.animate(withDuration: 0.3, animations: {
                v.backgroundColor = UIColor.gray
            })
        }
    }
}
